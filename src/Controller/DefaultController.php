<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/default', name: 'default')]
    public function index(): Response
    {
        return $this->render('default/account_list.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }
    #[Route('/', name: 'home')]
    public function home(): Response
    {
        return $this->render('default/home.html.twig', [
            'controller_name' => 'DefaultController',
            'title' => 'Home Page Of project'
        ]);
    }
    #[Route('/contact', name: 'contact')]
    public function contact(): Response
    {
        return $this->render('default/contact.html.twig', [
            'controller_name' => 'DefaultController',
            'title' => 'Formulaire de contact'
        ]);
    }
    #[Route('/contact_us', name: 'contact_us')]
    public function contactUs(Request $request)
    {
        try{
            $name = $request->get('name');
            $email = $request->get('email');
            $subjet = $request->get('subject');
            $message = $request->get('message');
            $msg = '';
            if ($name != '' && $email != '' && $subjet != '' && $message != '') {
                $this->addFlash("success", 'Message envoyé avec succès');
            }
            $this->addFlash("info", 'Une erreur est survenue durant l\'envoi du message');
            return $this->redirectToRoute('home',['message'=>'Votre message a bien été envoyé. Nous vous contacterons dès que possible.']);
        }catch (\Exception $e) {
            return new JsonResponse($e->getMessage());
        }
    }
    #[Route('/accounts', name: 'accounts')]
    public function allAccount(): Response
    {
        return $this->render('default/account_list.html.twig', [
            'title' => 'Adding an account'
        ]);
    }
    #[Route('/addNew', name: 'add_new')]
    public function addAccount(): Response
    {
        return $this->render('default/account.html.twig', [
            'title' => 'Adding an account'
        ]);
    }
}
