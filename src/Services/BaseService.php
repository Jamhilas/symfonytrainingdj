<?php


namespace App\Services;


use DateTime;

class BaseService
{
    public function isDateValid($date, $maxYear = 10) {
//        $dateConverted = DateTime::createFromFormat("d/m/Y", $date);
        // Get an array of the element to check the date
//        $dateToCheck = date_parse(date_format($dateConverted, 'Y-m-d'));
        $dateToCheck = date_parse(date_format($date, 'Y-m-d'));
        // The date is valid
        $isChecked = checkdate($dateToCheck["month"],$dateToCheck["day"],$dateToCheck["year"]);
        // Get the timestamp of th date entered
        $timestamp = $date->getTimestamp();
        // Get the curent timestamp
        $now = new DateTime();
        $nowTimestamp = $now->getTimestamp();
        // Check if the interval between the date entered and the current date is <= 150
        // If the interval is <= 150 return true else false
        $interval = round(($nowTimestamp - $timestamp)/(365*60*60*24));
        return ($interval<=$maxYear);
    }

}