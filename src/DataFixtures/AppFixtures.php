<?php

namespace App\DataFixtures;

use App\Entity\Personne;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 100; $i++) {
            $customer = new Personne();
            $customer->setNom($faker->firstName)
                     ->setPrenom($faker->lastName)
                     ->setDateNaissance($faker->dateTimeBetween('-150 years'));

            $manager->persist($customer);
        }

        $manager->flush();
    }
}
